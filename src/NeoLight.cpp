#include "NeoLight.h"

using namespace std;

NeoLight::NeoLight(uint16_t pixels, uint8_t pin, uint8_t type, void (*callback)()) : Adafruit_NeoPixel(pixels, pin, type)
{
    OnComplete = callback;
    current_mode = MODE_LIGHT;
    currentAnimationMode = MIN_ANIMATION_MODE;
}

void NeoLight::Update()
{

    if (current_mode == MODE_LIGHT)
    {
        // do nothing, color will be changed in changeMode()
    }
    else if (current_mode == MODE_ANIMATION)
    {
        changeAnimationMode();
        increaseCounter();
        resetCounter();
    }
}

void NeoLight::changeMode()
{
    shouldAnimationStop = true;

    if (current_mode == MODE_LIGHT)
    {
        Serial.println("switching to animation mode");
        current_mode = MODE_ANIMATION;
    }
    else
    {
        Serial.println("switching to light mode");
        current_mode = MODE_LIGHT;
        calculateColor();
    }
}

void NeoLight::increaseCounter()
{
    if (current_mode == MODE_ANIMATION)
    {
        if (currentAnimationMode == ANIMATION_MODE_0 || currentAnimationMode == ANIMATION_MODE_1)
        {
            currentPixelsPos++;
            currentPixelColor++;
        }
        else if (currentAnimationMode == ANIMATION_MODE_2)
        {
            if (animationDirection == 0)
            {
                additionalAnimationCounter++;
            }
            else if (animationDirection == 1)
            {
                additionalAnimationCounter--;
            }

            if (additionalAnimationCounter == 255)
            {
                animationDirection = 1;
                currentPixelColor = currentPixelColor + 5;
            }
            else if (additionalAnimationCounter == 0)
            {
                animationDirection = 0;
                currentPixelColor = currentPixelColor + 5;
            }

            currentPixelsPos++;
        }
    }
}

void NeoLight::resetCounter()
{
    if (current_mode == MODE_ANIMATION)
    {
        if (currentAnimationMode == ANIMATION_MODE_0 || currentAnimationMode == ANIMATION_MODE_1)
        {
            if (currentPixelsPos == numPixels())
                currentPixelsPos = 0;

            if (currentPixelColor == 255)
                currentPixelColor = 0;
        }
        else if (currentAnimationMode == ANIMATION_MODE_2)
        {
            if (currentPixelsPos == numPixels())
                currentPixelsPos = 0;

            if (currentPixelColor == 255)
            {
                animationDirection = 0;
                currentPixelColor = 0;
            }
        }
    }
}

void NeoLight::changeAnimationMode()
{
    shouldAnimationStop = false;

    switch (currentAnimationMode)
    {
    case ANIMATION_MODE_0:
        animationRainbow();
        break;
    case ANIMATION_MODE_1:
        animationChase();
        break;
    case ANIMATION_MODE_2:
        animationGlow();
        break;
    case ANIMATION_MODE_3:
        animationScanner();
        break;
    case ANIMATION_MODE_4:
        animationLava();
        break;

    default:
        break;
    }
}

void NeoLight::Increment()
{
    shouldAnimationStop = true;

    if (current_mode == MODE_LIGHT)
    {
        if (currentLightMode == MAX_LIGHT_MODE)
        {
            currentLightMode = MIN_LIGHT_MODE;
        }
        else
        {
            ++currentLightMode;
        }
        calculateColor();
    }
    else if (current_mode == MODE_ANIMATION)
    {
        resetCounter();

        if (currentAnimationMode == MAX_ANIMATION_MODE)
        {
            currentAnimationMode = MIN_ANIMATION_MODE;
        }
        else
        {
            ++currentAnimationMode;
        }
    }
}

void NeoLight::Reverse()
{
    shouldAnimationStop = true;

    if (current_mode == MODE_LIGHT)
    {
        if (currentLightMode == MIN_LIGHT_MODE)
        {
            currentLightMode = MAX_LIGHT_MODE;
        }
        else
        {
            --currentLightMode;
        }
        calculateColor();
    }
    else if (current_mode == MODE_ANIMATION)
    {
        resetCounter();

        if (currentAnimationMode == MIN_ANIMATION_MODE)
        {
            currentAnimationMode = MAX_ANIMATION_MODE;
        }
        else
        {
            --currentAnimationMode;
        }
    }
}

void NeoLight::ColorSet(uint32_t color)
{

    for (unsigned int i = 0; i < numPixels(); i++)
    {
        setPixelColor(i, color);
    }

    show();
}

void NeoLight::setIntensity(uint8_t val)
{
    setBrightness(val);
    show();
}

void NeoLight::calculateColor()
{
    switch (currentLightMode)
    {
    case LIGHNT_MODE_0:
        ColorSet(Color(255, 255, 255));
        break;
    case LIGHNT_MODE_1:
        ColorSet(Color(3, 255, 20));
        break;
    case LIGHNT_MODE_2:
        ColorSet(Color(0, 74, 236));
        break;
    case LIGHNT_MODE_3:
        ColorSet(Color(236, 0, 24));
        break;
    case LIGHNT_MODE_4:
        ColorSet(Color(255, 255, 0));
        break;
    case LIGHNT_MODE_5:
        ColorSet(Color(51, 103, 255));
        break;
    case LIGHNT_MODE_6:
        ColorSet(Color(127, 0, 255));
        break;
    case LIGHNT_MODE_7:
        ColorSet(Color(0, 204, 204));
        break;
    case LIGHNT_MODE_8:
        ColorSet(Color(153, 204, 255));
        break;
    case LIGHNT_MODE_9:
        ColorSet(Color(102, 102, 255));
        break;
    case LIGHNT_MODE_10:
        ColorSet(Color(255, 51, 153));
        break;
    }
}

void NeoLight::setCustomColor(uint8_t r, uint8_t g, uint8_t b)
{
    current_mode = MODE_LIGHT;
    currentLightMode = LIGHNT_MODE_0;
    ColorSet(Color(r, g, b));
}

uint32_t NeoLight::Wheel(byte WheelPos)
{
    WheelPos = 255 - WheelPos;
    if (WheelPos < 85)
    {
        return Color(255 - WheelPos * 3, 0, WheelPos * 3);
    }
    else if (WheelPos < 170)
    {
        WheelPos -= 85;
        return Color(0, WheelPos * 3, 255 - WheelPos * 3);
    }
    else
    {
        WheelPos -= 170;
        return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
    }
}

void NeoLight::animationChase()
{
    if (shouldAnimationStop)
        return;

    setPixelColor(currentPixelsPos, Wheel(currentPixelColor));
    delay(ANIMATION_CHASE_DELAY);

    if (currentPixelsPos != 0)
    {
        setPixelColor(currentPixelsPos - 1, 0);
    }
    if (currentPixelsPos - 1 != 0 && currentPixelsPos - 1 != -1)
        ;
    {
        setPixelColor(currentPixelsPos - 2, Wheel(currentPixelColor));
    }

    if (currentPixelsPos == (numPixels() - 1))
    {
        setPixelColor(0, 0);
    }
    else if (currentPixelsPos == 0)
    {
        setPixelColor(numPixels() - 1, 0);
    }

    show();

    if (OnComplete != NULL)
        OnComplete();
    if (shouldAnimationStop || shouldBeTurnedOff)
        return;
}

void NeoLight::animationRainbow()
{
    if (shouldAnimationStop)
        return;

    setPixelColor(currentPixelsPos, Wheel(currentPixelColor));
    delay(ANIMATION_RAINBOW_DELAY);
    show();

    if (OnComplete != NULL)
        OnComplete();
    if (shouldAnimationStop || shouldBeTurnedOff)
        return;
}

void NeoLight::animationGlow()
{
    if (shouldAnimationStop)
        return;

    setPixelColor(currentPixelsPos, Wheel(currentPixelColor));
    setBrightness(additionalAnimationCounter);

    show();

    if (OnComplete != NULL)
        OnComplete();
    if (shouldAnimationStop || shouldBeTurnedOff)
        return;
}

void NeoLight::animationScanner()
{
    // TODO
}

void NeoLight::animationLava()
{
    // TODO
}

void NeoLight::turnOn()
{
    shouldBeTurnedOff = false;

    setBrightness(100);

    if (current_mode == MODE_LIGHT)
    {
        calculateColor();
    }
}

void NeoLight::turnOff()
{
    shouldBeTurnedOff = true;
    setBrightness(0);
    show();
}
