#include <Arduino.h>
#include "NeoLight.h"
#include <SoftwareSerial.h>

using namespace std;

#define LED_PIN_E 2
#define LED_PIN_B 3
#define LED_PIN_O 4
#define LED_PIN_SH 5

#define NUMPIXELS_E 4
#define NUMPIXELS_B 4
#define NUMPIXELS_O 4
#define NUMPIXELS_SH 6

#define MAX_BL_COMMAND_LENGTH 100

const int
    animationDelay = 10,
    readBTDelay = 1;

char commandString[MAX_BL_COMMAND_LENGTH];
byte commandStringLen = 0;

void pixelsUpdateState();

NeoLight pixels_e(NUMPIXELS_E, LED_PIN_E, NEO_RGB + NEO_KHZ800, &pixelsUpdateState);
NeoLight pixels_b(NUMPIXELS_B, LED_PIN_B, NEO_RGB + NEO_KHZ800, &pixelsUpdateState);
NeoLight pixels_o(NUMPIXELS_O, LED_PIN_O, NEO_RGB + NEO_KHZ800, &pixelsUpdateState);
NeoLight pixels_sh(NUMPIXELS_SH, LED_PIN_SH, NEO_RGB + NEO_KHZ800, &pixelsUpdateState);

SoftwareSerial BTserial(8, 9); // RX | TX

void initPixels()
{
  pixels_e.begin();
  pixels_b.begin();
  pixels_o.begin();
  pixels_sh.begin();

  delay(animationDelay);

  pixels_e.turnOn();
  pixels_b.turnOn();
  pixels_o.turnOn();
  pixels_sh.turnOn();
}

void handleNextPixelsModeCommand()
{
  pixels_e.Increment();
  pixels_b.Increment();
  pixels_o.Increment();
  pixels_sh.Increment();
}

void handlePrevPixelsModeCommand()
{
  pixels_e.Reverse();
  pixels_b.Reverse();
  pixels_o.Reverse();
  pixels_sh.Reverse();
}

void handleChangePixelsModeCommand()
{
  pixels_e.changeMode();
  pixels_b.changeMode();
  pixels_o.changeMode();
  pixels_sh.changeMode();
}

void handleSetIntencity(uint8_t value)
{
  if (value > 254)
    value = 254;
  if (value <= 0)
    value = 0;

  pixels_e.setIntensity(value);
  pixels_b.setIntensity(value);
  pixels_o.setIntensity(value);
  pixels_sh.setIntensity(value);
}

void handleTurnOnPixelsCommand()
{
  pixels_e.turnOn();
  pixels_b.turnOn();
  pixels_o.turnOn();
  pixels_sh.turnOn();
}

void handleTurnOffPixelsCommand()
{
  pixels_e.turnOff();
  pixels_b.turnOff();
  pixels_o.turnOff();
  pixels_sh.turnOff();
}

void handleSetCustomColor(uint8_t r, uint8_t g, uint8_t b)
{
  pixels_e.setCustomColor(r, g, b);
  pixels_b.setCustomColor(r, g, b);
  pixels_o.setCustomColor(r, g, b);
  pixels_sh.setCustomColor(r, g, b);
}

bool readSerial()
{
  while ((BTserial.available()))
  {
    char inChar = BTserial.read();

    if (inChar == '\n' || inChar == '\r')
    {
      return true;
    }

    if (commandStringLen < (MAX_BL_COMMAND_LENGTH - 1))
    {
      commandString[commandStringLen++] = inChar;
      commandString[commandStringLen] = '\0';
    }
  }

  delay(readBTDelay);
  return false;
}

void commandParser()
{

  if (readSerial())
  {

    // handle commandString
    String commnad = String(commandString);

    Serial.println(commnad);

    if (commnad == "next")
    {
      handleNextPixelsModeCommand();
    }
    else if (commnad == "prev")
    {
      handlePrevPixelsModeCommand();
    }
    else if (commnad == "change_mode")
    {
      Serial.println("will change mode");
      handleChangePixelsModeCommand();
    }
    else if (commnad == "turnOn")
    {
      handleTurnOnPixelsCommand();
    }
    else if (commnad == "turnOff")
    {
      handleTurnOffPixelsCommand();
    }

    else if (commnad.startsWith("set_intensity#"))
    {
      if (commnad.length() != 17)
      {
        Serial.println("Invalid intensity string data");
        return;
      }

      handleSetIntencity(commnad.substring(14, 17).toInt());
    }

    else if (commnad.startsWith("set_color#"))
    {
      if (commnad.length() != 19)
      {
        Serial.println("Invalid color string data");
        return;
      }
      uint8_t r = commnad.substring(10, 13).toInt();
      uint8_t g = commnad.substring(13, 16).toInt();
      uint8_t b = commnad.substring(16, 19).toInt();

      if ( r > 254 ) r = 254;
      if ( g > 254 ) g = 254;
      if ( b > 254 ) b = 254;

      handleSetCustomColor(r, g, b);
    }
  }

  commandString[0] = '\0';
  commandStringLen = 0;
}

void setup()
{
  initPixels();

  Serial.begin(115200);
  Serial.println("Read AT commands:");

  BTserial.begin(115200);
}

void loop()
{
  commandParser();
  pixels_e.Update();
  pixels_b.Update();
  pixels_o.Update();
  pixels_sh.Update();
}

void pixelsUpdateState()
{
  commandParser();
}
